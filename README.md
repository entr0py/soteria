# soteria

A python based versatile RAT. (Remote Administration Tool) 

This project is a fork of Bucky Robert's reverse shell "turtle" (https://github.com/buckyroberts/turtle)

Function | Keylogging | Persistence | Webcam stuff
:------------ | :-------------| :-------------| :-------------
STATUS | :heavy_check_mark: |  :x:	 | :x:
